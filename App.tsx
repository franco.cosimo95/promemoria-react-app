import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Lists from './src/containers/Lists/Lists';
import ListDetail from './src/containers/ListDetail/ListDetail';
import Reminders from './src/containers/Reminders/Reminders';

/**
 * serve per poter gestire la navigazione
 */
const Stack = createStackNavigator();

/**
 * APP:
 * Utilizzo `react-navigation` per dichiarare le schermate
 * Ogni Screen ha nome della screen per identificarla successivamente
 * e componente associato
 *
 * WEB:
 * utilizzavo `react-router-dom` per dichiarare le rotte della web app
 *
 */
const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Lists" component={Lists} />
        <Stack.Screen name="ListDetail" component={ListDetail} />
        <Stack.Screen name="Reminders" component={Reminders} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
