import {List} from '../models/List';
import {Color} from '../models/Color.enum';
import {Reminder} from '../models/Reminder';
// @ts-ignore
import FireStoreParser from 'firestore-parser';

/**
 * la classe che si occupa di interfacciarsi con il Back End (Server) (Firebase)
 * CRUD: Create Read Update Delete
 */
export class ListController {
  private BASE_URL =
    'https://firestore.googleapis.com/v1/projects/promemoria-dae1b/databases/(default)/documents/';

  constructor() {}

  /**
   * Read array
   */
  getLists(): Promise<List[]> {
    /**
     * eseguo fetch che è una funzione javascript che mi permette di effettuare delle chiamate API
     * il primo parametro è l'url dell'API
     * il secondo paramentro è un oggetto che accetta la chiave 'method' con valore il metodo HTTP da utilizzare
     * in questo caso vogliamo ottenere i dati quindi il metodo è `GET`
     *
     * 1.`effettuo` una chiamata al server che mi ritorna una `response`
     * 2. prendo la risposta e la converto in JSON, ovverro oggetto javascript
     * 3. prendo l'oggetto javascript e trasformo ogni documento ricevuto in istanza di List
     * 4. ritorno il tutto come una Promise di array di List
     */
    return fetch(this.BASE_URL + 'lists', {method: 'GET'})
      .then((response: Response) => response.json())
      .then((json) => {
        /**
         * normalizzo il json di risposta utilizzando FireStoreParser, in modo da avere gli oggetti
         * json come chiave valore dove la chiave è l'attributo e il valore quello associato
         */
        FireStoreParser(json);
        if (json.documents) {
          return json.documents.map((document: any) => {
            console.log(document, JSON.stringify(document));
            return this.getListFromDoc(document);
          });
        } else {
          return [];
        }
      });
  }

  /**
   * Create element
   */
  createList(
    name: string,
    color?: Color,
    reminders?: Reminder[],
  ): Promise<List> {
    const body: any = this.getDocFromList(new List(name, color, reminders));
    /**
     * Uso il metodo POST per indicare che voglio creare un nuovo documento nella collection
     * JSON.stringify: trasforma un oggetto json in una stringa
     */
    return fetch(this.BASE_URL + 'lists', {
      method: 'POST',
      body: JSON.stringify(body),
    })
      .then((response) => response.json())
      .then((document) => {
        FireStoreParser(document);
        /**
         * prendo il documento che mi viene ritornato da firebase e lo trasformo in List in modo da ritornarlo
         */
        return this.getListFromDoc(document);
      });
  }

  /**
   * Update element
   */
  updateList(elenco: List): Promise<List> {
    const body = this.getDocFromList(elenco);
    return fetch(this.BASE_URL + 'lists/' + elenco.id, {
      method: 'PATCH',
      body: JSON.stringify(body),
    })
      .then((response) => response.json())
      .then((json) => {
        FireStoreParser(json);
        return this.getListFromDoc(json);
      });
  }

  /**
   * Delete element
   */
  deleteList(id: string): Promise<void> {
    /**
     * utilizzo il metodo DELETE per eliminare uno specifico documento già esistente
     */
    return fetch(this.BASE_URL + 'lists/' + id, {method: 'DELETE'}).then(
      (response) => undefined,
    );
  }

  private getDocumentId(name: string): string {
    /**
     * split: è un metodo della stringa che mi permette di creare un Array di stringhe dove
     * ogni elemento è il testo contenuto tra un separatore e un altro. Il separatore va passato come parametro.
     *
     * pop: rimuove l'ultimo elemento dall'array e me lo ritorna
     */
    return name.split('/').pop() || '';
  }

  /**
   * Mi permette di ottenere un oggetto adatto ad essere inviato a Firebase
   */
  private getDocFromList(list: List): any {
    return {
      fields: {
        name: {
          stringValue: list.name,
        },
        color: {
          stringValue: list.color,
        },
        reminders: {
          arrayValue: {
            values: list.reminders.map((r) => ({
              mapValue: {
                fields: {
                  name: {
                    stringValue: r.name,
                  },
                  description: {
                    stringValue: r.description,
                  },
                  position: {
                    stringValue: r.position,
                  },
                  date: {
                    stringValue: r.date,
                  },
                  done: {
                    booleanValue: !!r.done,
                  },
                },
              },
            })),
          },
        },
      },
    };
  }

  /**
   * prendo un documento Firebase e lo trasformo in istanza di List
   */
  private getListFromDoc(document: any): List {
    return new List(
      document.fields.name,
      document.fields.color,
      document.fields.reminders || [],
      this.getDocumentId(document.name),
    );
  }
}
