export interface IReminder {
  id: string;
  name: string;
  description?: string;
  date?: string;
  position?: string;
  done?: boolean;
}

export class Reminder implements IReminder {
  id: string;
  name: string;
  description?: string;
  date?: string;
  position?: string;
  done?: boolean;

  constructor(obj: IReminder) {
    this.id = obj.id;
    this.name = obj.name;
    this.description = obj.description;
    this.date = obj.date;
    this.position = obj.position;
    this.done = obj.done;
  }
}
