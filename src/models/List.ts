import {Reminder} from './Reminder';
import {Color} from './Color.enum';

export class List {
  id: string;
  name: string;
  color: Color;
  reminders: Reminder[];

  constructor(
    name: string,
    color: Color = Color.Red,
    reminders: Reminder[] = [],
    id: string = parseInt(Math.random() * 10000 + '', 10) + '',
  ) {
    this.id = id;
    this.name = name;
    this.color = color;
    this.reminders = reminders;
  }
}
