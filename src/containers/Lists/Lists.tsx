import React, {Component} from 'react';
import {FlatList, SafeAreaView, StyleSheet} from 'react-native';
import {List} from '../../models/List';
import {ListController} from '../../controllers/ListController';
import {Icon} from 'react-native-elements';
import ListItem from './components/ListItem/ListItem';

interface ListsProps {
  /**
   * viene passato come prop da react-navigation e serve per
   * navigare, impostare header, ...
   */
  navigation: any;
}

interface ListsState {
  elenchi: List[];
}

export default class Lists extends Component<ListsProps, ListsState> {
  listController: ListController = new ListController();
  unsubscribeFocusListener?: Function;

  constructor(props: ListsProps) {
    super(props);
    this.state = {
      elenchi: [],
    };
    this.setHeader();
  }

  componentDidMount(): void {
    /**
     * addListener è un metodo di navigation che mi permette di ascoltare
     * un determinato evento della navigazione.
     * In questo caso mi metto in ascolto dell'eveneto `focus` che viene chiamato
     * quando ritorno nella schermata; a questo punto riaggiorno la lista
     * degli elenchi così da averli sempre aggiornati dopo delle modifiche
     *
     * Il listener mi ritorna una funzione da chiamare quando devo distruggere
     * il componente `componentWillUnmount` in modo da smettere di ascoltare
     * quell'evento.
     *
     * Inizia la partita => inizio ad ascoltare i goal
     */
    this.unsubscribeFocusListener = this.props.navigation.addListener(
      'focus',
      () => {
        // Funzione chiamata quando c'è il goal
        this.fetchLists();
      },
    );
    this.fetchLists();
  }

  /**
   * Il componente sarà distrutto
   */
  componentWillUnmount() {
    if (this.unsubscribeFocusListener) {
      /**
       * smetto di ascoltare, faccio `unsubscribe` chiamando la funzione
       * ritornata dall' addListener
       */
      this.unsubscribeFocusListener();
    }
  }

  onActionPress(type: string): void {
    console.log('eseguito onActionPress', type);
    this.props.navigation.navigate('ListDetail', {isNew: true});
  }

  /**
   * SafeAreaView: va inserita per gestire l'eventuale spazio di uno
   * smartphone iOS (nuove versioni)
   *
   * FlatList: va utilizzata quando bisogna mostrare una lista di elementi
   * uguali come grafica (es. la cella del nostro elenco). Permette di
   * ottimizzare le performance della APP in modo da riciclare le celle
   * come richiesto dal mondo nativo.
   */
  render() {
    return (
      <SafeAreaView>
        <FlatList
          numColumns={2}
          data={this.state.elenchi}
          renderItem={(item) => (
            <ListItem
              isLast={
                item.index === this.state.elenchi.length - 1 &&
                this.state.elenchi.length % 2 !== 0
              }
              key={item.item.id}
              list={item.item}
              onListPress={() => this.onListPress(item.item)}
            />
          )}
        />
      </SafeAreaView>
    );
  }

  /**
   * utilizzo navigation per impostare le azioni sulla destra
   * dell'header e il titolo da mostrare
   */
  private setHeader() {
    this.props.navigation.setOptions({
      /**
       * associo il titolo da mostrare nell'Header
       */
      headerTitle: 'Elenchi',
      /**
       * ritorno l'icona da mostrare sulla destra
       *
       * Icon è il componente delle icone che usiamo
       * grazie alla libreria react-native-elements
       */
      headerRight: () => {
        return (
          <Icon
            type="font-awesome-5"
            name="plus"
            color="#217dff"
            size={20}
            iconStyle={styles.icon}
            onPress={(event) => {
              console.log(event);
              this.onActionPress('plus');
            }}
          />
        );
      },
    });
  }

  /**
   * effettuo la chiamata per ottenere gli elenchi
   */
  private fetchLists() {
    this.listController
      .getLists()
      .then((lists) => {
        this.setState({elenchi: lists});
      })
      .catch((error) => {
        console.error('Impossibile ottenere la lista!', error);
      });
  }

  /**
   * apro la nuova schermata passando delle prop alla navigazione
   */
  private onListPress(item: List) {
    this.props.navigation.navigate('Reminders', {list: item});
  }
}

const styles = StyleSheet.create({
  icon: {
    marginRight: 20,
  },
});
