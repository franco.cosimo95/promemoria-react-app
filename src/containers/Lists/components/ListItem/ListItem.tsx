import React from 'react';
import {List} from '../../../../models/List';
import {StyleSheet, Text, View} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';

interface ListItemProps {
  list: List;
  onListPress: () => void;
  isLast: boolean;
}

export default function ListItem(props: ListItemProps) {
  return (
    <>
      <View style={styles.list} onTouchEnd={() => props.onListPress()}>
        <View
          style={{...styles.listColor, backgroundColor: props.list.color}}
        />
        <View style={styles.listBody}>
          <Text numberOfLines={2} style={styles.listName}>
            {props.list.name}
          </Text>
          <Text style={styles.listCount}>{props.list.reminders?.length}</Text>
        </View>
      </View>
      {props.isLast && <View style={styles.emptyCell} />}
    </>
  );
}

/**
 * non abbiamo bisogno di specificare `display: flex` perchè
 * Flexbox è utilizzato di default
 *
 * non utilizzo i `px`, ma fornisco valore numerico che sono i `pt`
 * utilizzati dal nativo
 *
 * `elevation` serve per dare profondità all'elemento e quindi assegnare
 * un'ombra automatica (Android)
 */
const styles = StyleSheet.create({
  list: {
    backgroundColor: Colors.white,
    borderRadius: 8,
    margin: 10,
    padding: 10,
    elevation: 8,
    flex: 1,
    flexDirection: 'column',
    // quello che segue è l'ombra su iOS
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.17,
    shadowColor: 'black',
  },
  listColor: {
    height: 30,
    width: 30,
    borderRadius: 15,
  },
  listBody: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 20,
  },
  listName: {
    maxWidth: '80%',
    fontSize: 14,
    lineHeight: 16,
    paddingRight: 20,
    height: 32,
    overflow: 'hidden',
    fontWeight: '600',
    fontFamily: 'Helvetica',
    color: '#454545',
  },
  listCount: {
    fontSize: 14,
    lineHeight: 16,
    fontWeight: 'bold',
    color: '#9A9A9A',
  },
  emptyCell: {
    flex: 1,
    margin: 10,
    padding: 10,
  },
});
