import React from 'react';
import {FlatList, SafeAreaView, StyleSheet, Text} from 'react-native';
import {List} from '../../models/List';
import {Icon} from 'react-native-elements';

interface RemindersProps {
  navigation: any;
  route: {
    params: {
      list: List;
    };
  };
}

export default function Reminders(props: RemindersProps) {
  function setHeader(): void {
    props.navigation.setOptions({
      headerTitle: props.route.params.list.name,
      headerRight: () => (
        <Icon
          type="font-awesome-5"
          name="cog"
          color="#217dff"
          size={20}
          iconStyle={styles.icon}
          onPress={() =>
            props.navigation.navigate('ListDetail', {
              elenco: props.route.params.list,
            })
          }
        />
      ),
    });
  }

  setHeader();

  return (
    <SafeAreaView>
      {props.route.params.list.reminders.length ? (
        <FlatList
          data={props.route.params.list.reminders}
          renderItem={(item) => (
            <Text key={item.item.id}>{item.item.name}</Text>
          )}
        />
      ) : (
        <Text>Questo elenco non contiene promemoria</Text>
      )}
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  icon: {
    marginRight: 20,
  },
});
