import React, {Component} from 'react';
import {
  Button,
  FlatList,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';
import {List} from '../../models/List';
import {Color} from '../../models/Color.enum';
import {Icon} from 'react-native-elements';
import {ListController} from '../../controllers/ListController';

interface ListDetailProps {
  /**
   * react-navigation passa navigation e route che contiene le prop passate
   * dalla navigazione
   */
  navigation: any;
  route: {
    params: {
      isNew?: boolean;
      elenco?: List;
    };
  };
}

interface ListDetailState {
  name: string;
  color: Color;
  colors: Color[];
  loading: boolean;
}

export default class ListDetail extends Component<
  ListDetailProps,
  ListDetailState
> {
  listController: ListController = new ListController();

  constructor(props: ListDetailProps) {
    super(props);
    // @ts-ignore
    const colors: Color[] = Object.values(Color);
    this.state = {
      name: props.route.params.elenco ? props.route.params.elenco.name : '',
      color: props.route.params.elenco
        ? props.route.params.elenco.color
        : colors[0],
      colors: colors,
      loading: false,
    };
    this.setHeader();
  }

  private setHeader() {
    this.props.navigation.setOptions({
      headerTitle: this.props.route.params.isNew
        ? 'Nuovo elenco'
        : 'Modifica elenco',
      headerRight: () => {
        return this.props.route.params.isNew ? undefined : (
          <Icon
            type="font-awesome-5"
            name="trash"
            color="#217dff"
            size={20}
            iconStyle={styles.icon}
            onPress={() => this.onDelete()}
          />
        );
      },
    });
  }

  onSave(): void {
    this.setState({loading: true});
    if (this.props.route.params.isNew) {
      this.listController
        .createList(this.state.name, this.state.color)
        .then(() => {
          this.setState({loading: false});
          this.props.navigation.navigate('Lists');
        })
        .catch((error) => {
          console.error(error);
          this.setState({loading: false});
        });
    } else {
      const elenco = this.props.route.params.elenco;
      if (elenco) {
        elenco.name = this.state.name;
        elenco.color = this.state.color;
        this.listController
          .updateList(elenco)
          .then(() => {
            this.setState({loading: false});
            this.props.navigation.navigate('Lists');
          })
          .catch((error) => {
            console.error(error);
            this.setState({loading: false});
          });
      }
    }
  }

  onDelete(): void {
    if (this.props.route.params.elenco) {
      this.setState({loading: true});
      this.listController
        .deleteList(this.props.route.params.elenco.id)
        .then(() => {
          this.setState({loading: false});
          this.props.navigation.navigate('Lists');
        })
        .catch((error) => {
          this.setState({loading: false});
          console.error(error);
        });
    }
  }

  render() {
    return (
      <SafeAreaView>
        <View style={styles.container}>
          <View style={styles.formInput}>
            <Text style={styles.label}>Nome elenco</Text>
            <TextInput
              value={this.state.name}
              style={styles.input}
              onChangeText={(text) => this.setState({name: text})}
            />
          </View>
          <View style={styles.formInput}>
            <Text style={styles.label}>Colore</Text>
            <FlatList
              data={this.state.colors}
              numColumns={5}
              columnWrapperStyle={styles.colors}
              renderItem={(c) => {
                /**
                 * duplico l'oggetto styles.color e lo assegno a style
                 * in modo da poterlo modificare
                 *
                 * aggiorno lo stile e lo passo come style
                 */
                const style: any = {...styles.color};
                style.backgroundColor = c.item;
                if (c.item === this.state.color) {
                  style.borderColor = 'black';
                  style.borderWidth = 2;
                }
                return (
                  <View
                    key={c.item}
                    style={style}
                    onTouchEnd={() => {
                      this.setState({color: c.item});
                    }}
                  />
                );
              }}
            />
          </View>
        </View>
        <View style={styles.footer}>
          <Button
            disabled={this.state.name.trim().length === 0 || this.state.loading}
            title="Salva"
            color="#217dff"
            onPress={() => this.onSave()}>
            Salva
          </Button>
        </View>
      </SafeAreaView>
    );
  }
}

/**
 * StyleSheet.create => mi permette di creare un oggetto di stile
 * i valori non vanno specificati in px, automaticamente sono in punti
 * non esistono selettori (id, classe), lo stile viene assegnato inline
 * di default viene utilizzato flexbox per il positioning
 *
 * dichiaro uno StyleSheet che DEVE essere utilizzato per
 * stilizzare i componenti, NON POSSO USARE i file css,
 * ma lo stile è un css camelCase: la maggior parte delle proprietà
 * css sono uguali a quelle viste sul web.
 */
const styles = StyleSheet.create({
  icon: {
    marginRight: 20,
  },
  container: {
    minHeight: '100%',
    backgroundColor: 'white',
    padding: 20,
  },
  input: {
    borderColor: '#979797',
    borderWidth: 1,
    borderRadius: 4,
    height: 45,
    padding: 10,
  },
  label: {
    fontWeight: 'bold',
    fontSize: 12,
    marginBottom: 10,
    color: '#3c3c4399',
  },
  formInput: {
    marginBottom: 20,
  },
  colors: {
    justifyContent: 'space-between',
  },
  color: {
    height: 35,
    width: 35,
    /**
     * calcolo a mano il 50% perchè in react native devo dare un valore numerico
     */
    borderRadius: 35 / 2,
    marginBottom: 20,
  },
  footer: {
    padding: 20,
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
  },
});

// export default function ListDetail() {
//   return (
//     <SafeAreaView>
//       <View>
//         <Text>List Detail</Text>
//       </View>
//     </SafeAreaView>
//   );
// }
